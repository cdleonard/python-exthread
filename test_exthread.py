import pprint
import sys
import traceback
from logging import getLogger

import pytest

from exthread import ExThread

logger = getLogger(__name__)


def test_one():
    def thread_run():
        raise Exception("Hello")

    t = ExThread(target=thread_run)
    t.start()

    ei = None
    try:
        t.join()
    except:
        ei = sys.exc_info()
    assert ei is not None

    logger.debug("got", exc_info=ei)
    assert ei[0] == Exception
    assert str(ei[1]) == "Hello"
    tb = traceback.extract_tb(ei[2])
    logger.debug("%s", pprint.pformat(tb))

    found_inner_traceback = False
    for tbe in tb:
        if "thread_run" in tbe[2]:
            found_inner_traceback = True
    assert found_inner_traceback


def test_raise_on_join_false():
    def thread_run():
        raise Exception("Hello")

    t = ExThread(target=thread_run, raise_inner_on_join=False)
    t.start()
    t.join()
    assert t.inner_exception is not None
    with pytest.raises(Exception):
        t.raise_inner()


def test_noraise():
    def thread_run():
        pass

    t = ExThread(target=thread_run)
    t.start()
    t.join()
    t.raise_inner()
    assert t.inner_exception is None
