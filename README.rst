.. SPDX-License-Identifier: MIT

Exception-Propagating Thread
============================

This is a small python module providing an derived implementation of the Thread
class which catches exceptions from `run` and reports them on `join`.

Testing
-------

* Run ``tox`` or ``pytest``
